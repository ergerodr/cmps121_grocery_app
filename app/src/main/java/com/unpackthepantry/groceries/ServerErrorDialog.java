package com.unpackthepantry.groceries;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

/**
 * Created by ergerodr on 02/03/17.
 */

public class ServerErrorDialog extends DialogFragment {
    public interface ServerDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog);
        //public void onDialogNegativeClick(DialogFragment dialog);
    }
    //use this instance of the interface to deliver action events
    ServerDialogListener mListener;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        return builder.setMessage(R.string.login_connection_error)
                .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Use token here to authenticate cached user and retrieve items in db.
                        mListener.onDialogPositiveClick(ServerErrorDialog.this);
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                }).create();
    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        //Verify that the host activity implements the callback interface
        try{
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (ServerDialogListener) activity;
        } catch (ClassCastException e){
            //The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement ServerDialogListener");
        }
    }
}
