package com.unpackthepantry.groceries;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;


import java.io.File;
import java.util.UUID;


/**
 * Created by ergerodr on 3/3/16.
 */
public class GroceryFragment extends Fragment {

    private static final String ARG_GROCERY_ID = "grocery_id";
    private static final String ARG_QUANTITY = "item_quantity";
    public final String APP_TAG = "MyCustomApp";
    public final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1034;
    public String photoFileName = "photo.jpg";

    private ImageView itemPicture;
    private EditText itemNameView;
    private TextView itemOwnerView;
    private CheckBox communalChkBox;
    private TextView itemFreshnessView;
    private TextView itemQuantityView;
    private Button deleteButton;
    private Button setImageButton;
    private File mPhotoFile;

    private Spinner quantitySpinner;

    private GroceryItem mGroceryItem;

    public static GroceryFragment newInstance(UUID groceryId){
        Bundle args = new Bundle();
        args.putSerializable(ARG_GROCERY_ID, groceryId);
        GroceryFragment fragment = new GroceryFragment();
        fragment.setArguments(args);//attach Bundle and saved info to fragment
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        //Retrieve arguments from bundle. Saved in "newInstance(UUID)" method above.
        UUID groceryId = (UUID) getArguments().getSerializable(ARG_GROCERY_ID);
        mGroceryItem = GroceryManager.get(getActivity()).getGroceryItem(groceryId);
        mPhotoFile = GroceryManager.get(getActivity()).getPhotoFile(mGroceryItem);


    }

    @Override
    public void onResume(){
        itemPicture.setImageBitmap(mGroceryItem.getBitmap());
        super.onResume();
    }

    @Override
    public void onPause(){
        itemPicture.setImageBitmap(mGroceryItem.getBitmap());
        super.onPause();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState){

        View v = inflater.inflate(R.layout.fragment_grocery, container, false);

        itemNameView = (EditText) v.findViewById(R.id.groceryNameView);
        itemNameView.setText(mGroceryItem.getItemName());
        itemNameView.addTextChangedListener(new TextWatcher() { //Set new Item name if user changes field
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mGroceryItem.setItemName(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        communalChkBox = (CheckBox) v.findViewById(R.id.communalCheckBox);
        communalChkBox.setChecked(mGroceryItem.isCommunal());
        communalChkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //right now anyone can check if something is communal, Need to reserve this for only
                //The OWNER of that item.
                mGroceryItem.setCommunal(isChecked);
            }
        });

        itemFreshnessView = (TextView) v.findViewById(R.id.detailDayCounter);
        itemFreshnessView.setText(getString(R.string.detailDaysOld,
                String.valueOf(0)));

        itemQuantityView = (TextView) v.findViewById(R.id.itemQuantityView);
        itemQuantityView.setText(getString(R.string.numItems, mGroceryItem.getQuantity()));

        deleteButton = (Button) v.findViewById(R.id.deleteItemButton);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GroceryManager.get(getActivity()).deleteGroceryItem(mGroceryItem.getId());
                closeFragment();
            }
        });

        //Need to check for valid photo application.
        setImageButton = (Button) v.findViewById(R.id.setImageButton);
        setImageButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                onLaunchCamera(v);
            }
        });

        itemPicture = (ImageView) v.findViewById(R.id.itemImageView);
        itemPicture.setImageBitmap(mGroceryItem.getBitmap());

        quantitySpinner = (Spinner) v.findViewById(R.id.quantitySpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter
                .createFromResource(getContext(), R.array.values, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        quantitySpinner.setAdapter(adapter);
        quantitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.i("In"," OnItemSelcted");
                String value = quantitySpinner.getSelectedItem().toString();
                if (!value.equals("Change")) {
                    itemQuantityView.setText(getString(R.string.numItems, value));
                    mGroceryItem.setQuantity(Integer.parseInt(value));
                }
                setSpinnerToValue(quantitySpinner, value);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.i("In","onNothingSelected");
                CharSequence quantity = savedInstanceState.getCharSequence(ARG_QUANTITY);
                mGroceryItem.setQuantity(Integer.parseInt(quantity.toString()));
            }
        });
        return v;
    }

    public void setSpinnerToValue(Spinner spinner, String value) {
        int index = 0;
        SpinnerAdapter adapter = spinner.getAdapter();
        for (int i = 0; i < adapter.getCount(); i++) {
            if (adapter.getItem(i).equals(value)) {
                index = i;
                break; // terminate loop
            }
        }
        spinner.setSelection(index);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE){
            if(resultCode == getActivity().RESULT_OK){
                Uri takenPhotoUri = getPhotoFileUri(photoFileName);
                //Camera photo is now on disk
                Bitmap takenImage = BitmapFactory.decodeFile(takenPhotoUri.getPath());
                //resize Bitmap
                //Load the taken image into a preview
                itemPicture = (ImageView) getActivity().findViewById(R.id.itemImageView);
                Bitmap imgScaled = Bitmap.createScaledBitmap(takenImage,itemPicture.getWidth()
                        ,itemPicture.getHeight(), true);

                mGroceryItem.setBitmap(imgScaled);
                itemPicture.setImageBitmap(imgScaled);
            }
        }
    }
    private void closeFragment(){
        /* Need to pop off most recent fragment.
        Fragment newFragment = new GroceryListFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.fragment_container, newFragment);
        transaction.addToBackStack(null);

        transaction.commit();
        */
    }
    public void onLaunchCamera(View view){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, getPhotoFileUri(photoFileName)); //set image filename

        // If you call startActivityForResult() using an intent that no app can handle, This will crash
        if(intent.resolveActivity(getActivity().getPackageManager()) != null){
            startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
        }
    }

    public Uri getPhotoFileUri(String fileName){
        // Only continue if the SD Card is mounted.
        if(isExternalStorageAvailable()){
            // Get safe Storage Directions for photos
            // Use 'getExternalFilesDir' on Context to access package-specific directories.
            // This way, we don't need to request external read/write runtime permissions.
            File mediaStorageDir = new
                    File(getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES), APP_TAG);

            //Create the storage directory if it does not exist.
            if(!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()){
                Log.d(APP_TAG, "Failed to create directory");
            }

            // Return the file target for the photo based on filename
            return Uri.fromFile(new File(mediaStorageDir.getPath() + File.separator + fileName));
        }
        return null;
    }

    //returns true if extornal storage for photos is available
    private boolean isExternalStorageAvailable(){
        String state = Environment.getExternalStorageState();
        return state.equals(Environment.MEDIA_MOUNTED);
    }

}
