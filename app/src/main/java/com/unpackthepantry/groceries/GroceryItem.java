package com.unpackthepantry.groceries;

import android.graphics.Bitmap;
import android.util.Log;

import java.util.UUID;

/**
 * Created by ergerodr on 3/2/16.
 */
public class GroceryItem {

    //Unique Grocery Identifier
    private UUID mId;
    //Owner Identifier
    private UUID mOwnerId;
    private String mItemName;
    private int mQuantity;
    private boolean mCommunal;
    private Bitmap mBitmap;

    @Override
    public String toString(){
        String result = "";
        result += mItemName + ",";
        result += " " + Integer.toString(mQuantity) + " remaining, ";
        result += "Communal: " + String.valueOf(mCommunal) + ", ";
        return result;
    }

    @Override
    public boolean equals(Object obj){
        if(obj instanceof GroceryItem){
            return (this.mId.toString().equals(((GroceryItem) obj).mId.toString()));
        } else {
            return false;
        }
    }

    public int getQuantity() {
        return mQuantity;
    }

    public void setQuantity(int quantity) {
        Log.i("SETTING QUANTITY TO ", String.valueOf(quantity));
        mQuantity = quantity;
    }

    public UUID getId() {
        return mId;
    }

    public void setId(UUID id) {
        mId = id;
    }

    public UUID getmOwnerId() {
        return mOwnerId;
    }

    public void setmOwnerId(UUID mOwnerId) {
        this.mOwnerId = mOwnerId;
    }

    public boolean isCommunal() {
        return mCommunal;
    }

    public void setCommunal(boolean communal) {
        mCommunal = communal;
    }

    public String getItemName() {
        return mItemName;
    }

    public void setItemName(String caption) {
        mItemName = caption;
    }

    public Bitmap getBitmap() {
        return mBitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        mBitmap = bitmap;
    }

    public String getPhotoFilename(){
        return "IMG_" + getId().toString() + ".jpg";
    }

    public GroceryItem (){
        mId = UUID.randomUUID();
        mCommunal = false;
        mItemName = "";
        mQuantity = 0;
    }

    public GroceryItem(UUID id, String itemName, int quantity, boolean communal, UUID owner){
        mOwnerId = owner;
        mCommunal = communal;
        mItemName = itemName;
        mQuantity = quantity;
        mId = id;
    }

}
