package com.unpackthepantry.groceries;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Borrowed from Luca's code for generating random strings.
 */
public class SecureRandomString {
    private SecureRandom random = new SecureRandom();

    public String nextString() {
        return new BigInteger(130, random).toString(32);
    }
}
