package com.unpackthepantry.groceries;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.DialogFragment;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.http.SslError;
import android.nfc.Tag;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

public class MainActivity extends AppCompatActivity
                          implements ServerErrorDialog.ServerDialogListener{

    boolean activeUser;
    boolean authenticated = false;
    String userToken= "";

    private Button groceryButton;
    private WebView myWebView;
    private Toolbar mToolbar;
    // Use URL below for production
    //private final String loginUrl = "https://960.aaramirez.tk/api/groceriesapi/login";
    //private final String logoutUrl = "https://960.aaramirez.tk/api/default/user/logout";
    // Use URL below for testing purposes
    private final String loginUrl = "http://192.168.50.100:8000/groceriesapi/groceriesapi/login";
    private final String logoutUrl = "http://192.168.50.100:8000/groceriesapi/default/user/logout" +
            "?_next=/groceriesapi/groceriesapi/login";

    private final String LOG_TAG = "MainActivity";
    private SharedPreferences sharedPref;



    @Override
    protected void onStart() {
        super.onStart();
        Context context = getApplicationContext();
        sharedPref = context.getSharedPreferences(getString(R.string.preferenceFile), Context.MODE_PRIVATE);
    }

    public void destroyWebView(){
        //LinearLayout parent = (LinearLayout) findViewById(R.id.parent_layout);
        //parent.removeView(myWebView);
        //WebView test = (WebView) findViewById(R.id.webView);
        // Simply hide the webview!
        myWebView.setVisibility(View.GONE);
    }

    public void destroyDefaultButtons(){
        groceryButton.setVisibility(View.INVISIBLE);
        groceryButton.setClickable(false);
        Button logout = (Button) findViewById(R.id.logoutBtn);
        logout.setVisibility(View.INVISIBLE);
        logout.setClickable(false);
    }

    public void setDefaultButtons(){
        groceryButton.setVisibility(View.VISIBLE);
        groceryButton.setClickable(true);
        Button logout = (Button) findViewById(R.id.logoutBtn);
        logout.setVisibility(View.VISIBLE);
        logout.setClickable(true);
    }

    @Override
    protected void onPause(){
        super.onPause();
        if(authenticated){
            destroyWebView();
            setDefaultButtons();
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        if(authenticated){
            destroyWebView();
            setDefaultButtons();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        groceryButton = (Button) findViewById(R.id.groceryButton);
        groceryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activeUser) {
                    Intent intent = new Intent(MainActivity.this, GroceryListActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(MainActivity.this, userSetup.class);
                    startActivity(intent);
                }
            }
        });
        myWebView = (WebView) findViewById(R.id.webView);
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        //Binding the Javascript interface
        myWebView.addJavascriptInterface(new JavascriptInterface(this), "Android");
        myWebView.setWebChromeClient(new WebChromeClient());
        myWebView.setWebViewClient(new SSLWorkAroundWebViewClient());
        myWebView.loadUrl(loginUrl);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
    }

    public class JavascriptInterface {
        Context mContext;

        JavascriptInterface(Context c) {
            mContext = c;
        }
        @android.webkit.JavascriptInterface
        public void setParams(String token, String valid, String name) {
            Log.i(LOG_TAG, "Valid: " + valid);
            activeUser = valid.equals("True");
            Log.i(LOG_TAG, "Token: " + token);
            userToken = token;
            authenticated = true;
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("userToken", userToken);
            editor.commit();
        }
    }

    private class SSLWorkAroundWebViewClient extends WebViewClient {
        //Workaround needed since server needs HTTPS but I don't have valid
        //SSL Cert
        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
           // super.onReceivedSslError(view, handler, error);
            handler.proceed();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // Once user is logged in forward to correct activity
            if (authenticated) {
                if (activeUser) {
                    Log.i(LOG_TAG, "User associated with house");
                    Intent intent = new Intent(MainActivity.this, GroceryListActivity.class);
                    startActivity(intent);
                } else {
                    Log.i(LOG_TAG, "User not associated with house");
                    Intent intent = new Intent(MainActivity.this, userSetup.class);
                    startActivity(intent);
                }
            } else {
                // if the view is hidden, enable it if the user has been logged out
                if (myWebView.getVisibility() == View.GONE) {
                    myWebView.setVisibility(View.VISIBLE);
                }
            }
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error){
            view.setVisibility(View.INVISIBLE);
            super.onReceivedError(view, request, error);
            ServerErrorDialog dialog = new ServerErrorDialog();
            dialog.show(getFragmentManager(), LOG_TAG);
        }

        @Override //Returning false here allows the webview to handle URL loading, not an external browser
        public boolean shouldOverrideUrlLoading(WebView webView, String request){
            super.shouldOverrideUrlLoading(webView, request);
            return false;
        }
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog){
        myWebView.setVisibility(View.VISIBLE);
        myWebView.clearCache(true);
        myWebView.loadUrl("about:blank");
        myWebView.loadUrl(loginUrl);
        dialog.dismiss();
    }

    public void logOut(View v) {
        authenticated = false;
        destroyDefaultButtons();
        myWebView.loadUrl(logoutUrl);
    }
}


