package com.unpackthepantry.groceries.sync;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.util.Log;

import com.unpackthepantry.groceries.GroceryItem;
import com.unpackthepantry.groceries.GroceryManager;
import com.unpackthepantry.groceries.database.GroceryContract;
import com.unpackthepantry.groceries.groceryService;
import com.unpackthepantry.groceries.response.GetItemsResponse;
import com.unpackthepantry.groceries.response.ItemList;


import java.io.IOException;
import java.util.List;
import java.util.UUID;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by ergerodr on 12/24/16.
 */

public class SyncAdapter extends AbstractThreadedSyncAdapter {
    // Global variables
    // Define a variable to contain a content resolver instance
    ContentResolver mContentResolver;
    private HttpLoggingInterceptor logging;
    private OkHttpClient httpClient;
    Retrofit retrofit;
    groceryService service;

    private final String baseUrl = "http://192.168.50.100:8000/groceriesapi/groceriesapi/";
    private static final String LOG_TAG = "SyncAdapter";
    public static final String SYNC_MESSAGE = "com.unpackthepantry.groceries.sync.SYNC_MESSAGE";
    public static final String SYNC_ACTION_ADD = "com.unpackthepantry.groceries.sync.ADD_ITEM";
    public static final String SYNC_RESULT = "com.unpackthepantry.groceries.sync.SYNC_PROCESSED";
    /**
     * Set up the sync adapter constructor
     */
    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        mContentResolver = context.getContentResolver();
    }
    /**
     * Set up the 2nd sync adapter constructor. This form of the
     * constructor maintains compatibility with Android 3.0
     * and later platform versions
     */
    public SyncAdapter(
            Context context,
            boolean autoInitialize,
            boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        mContentResolver = context.getContentResolver();
    }
    /*
     * Specify the code you want to run in the sync adapter. The entire
     * sync adapter runs in a background thread, so you don't have to set
     * up your own background processing.
     */
    @Override
    public void onPerformSync(Account account,
                              Bundle extras,
                              String authority,
                              ContentProviderClient provider,
                              SyncResult syncResult) {
    /*
     * Put the data transfer code here.
     *
     *   1) Connect to server
     *   2) Download data and insert into provider
     *      -Send any data if necessary.
     *      -Handle any network errors if necessary
     *   3) Handle data conflicts or determine how current the data is.
     *   4) Close connections(clean up)
     */
        String userToken = extras.getString("userToken");
        String houseId = extras.getString("houseId");

        // Create HTTP Interface
        logging = new HttpLoggingInterceptor();
        // Set the desired logging level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();
        retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();
        service = retrofit.create(groceryService.class);

        //Set up of SYNCHRONOUS retrofit call.
        Call<GetItemsResponse> getItemsCall = service.getItems(userToken, houseId);
        Response<GetItemsResponse> response;
        try {
            response = getItemsCall.execute();
        }catch (IOException ioe) {
            Log.d(LOG_TAG, ioe.toString());
            response = null;
        }
        if(response != null) {
            if (response.body().getResult().equals("ok")) {
                Log.i(LOG_TAG, "Items received from server!");
                List<ItemList> houseItems = response.body().getItemList();
                GroceryManager groceryManager = GroceryManager.get(getContext());
                List<GroceryItem> groceryItems = groceryManager.getGroceryItems();
                for (int i = 0; i < houseItems.size(); i++) {
                    GroceryItem item = new GroceryItem(UUID.fromString(houseItems.get(i).getGroceryId() + "1234567-9ABC-DEF0-1234-56789ABCDEF0"),
                            houseItems.get(i).getItemName(),
                            Integer.parseInt(houseItems.get(i).getNumRemaining()),
                            houseItems.get(i).getCommunal(),
                            UUID.fromString(houseItems.get(i).getOwnerId()));
                    Log.i(LOG_TAG, item.toString());
                    if (!groceryItems.contains(item)) { //slow way to prevent duplicates, I KNOW I SHOULD BE USING A SET,
                        Log.i(LOG_TAG, item.getId().toString());
                        groceryManager.addGrocery(item); //BUT I DON'T WANT TO YET SINCE THIS IS TO TEST SOMETHING FIRST.
                    }
                }
                sendSyncResult(SYNC_RESULT);
            }
        }
    }

    private void sendSyncResult(String message){
        Log.d(LOG_TAG, "Sending Sync Result!");
        getContext().getContentResolver().notifyChange(GroceryContract.BASE_CONTENT_URI, null, false);
    }
}

/* A ContentProviderClient is a lightweight interface to a content provider. It has the
*   same basic functionality as a ContentResolver
*
 */
