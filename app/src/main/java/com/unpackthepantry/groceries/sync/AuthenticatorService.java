package com.unpackthepantry.groceries.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Created by ergerodr on 12/24/16.
 */

public class AuthenticatorService extends Service {
    // Instance field that stores the authenticator object
    private Authenticator mAuthenticator;
    @Override
    public void onCreate() {
        // Create a new authenticator object
        mAuthenticator = new Authenticator(this);
    }
    /*
     * When the system binds to this Service to make the RPC call
     * return the authenticator's IBinder.
     *
     * An IBinder is just a basic interface for a remote object.
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mAuthenticator.getIBinder();
    }
}
