package com.unpackthepantry.groceries;

import com.unpackthepantry.groceries.response.BasicResponse;
import com.unpackthepantry.groceries.response.GetItemsResponse;
import com.unpackthepantry.groceries.response.HouseInfoResponse;
import com.unpackthepantry.groceries.response.PostItemsRequest;
import com.unpackthepantry.groceries.response.ServerResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Aaron Ramirez on 0011 Mar 11 2016.
 */
public interface groceryService {
    @GET("add_house")
    Call<BasicResponse> addHouse(@Query("userToken") String userToken,
                                  @Query("houseId") String houseId,
                                  @Query("nickname") String nickname);

    @GET("find_house")
    Call<ServerResponse> findHouse(@Query("userToken") String userToken,
                                   @Query("nickname") String nickname);

    @GET("get_house_info")
    Call<HouseInfoResponse> getHouseInfo(@Query("userToken") String userToken);

    @GET("add_user")
    Call<ServerResponse> addUser(@Query("userToken") String userToken,
                                 @Query("houseId") String houseId);


    @GET("get_items")
    Call<GetItemsResponse> getItems(@Query("userToken") String userToken,
                                     @Query("houseId") String houseId);

    @FormUrlEncoded
    @POST("add_user_item")
    Call<PostItemsRequest> addUserItem(@Field("userToken") String userToken,
                                       @Field("itemName") String itemName,
                                       @Field("numRemaining") int numRemaining,
                                       @Field("communal") boolean communal);
}
