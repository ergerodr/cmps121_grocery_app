package com.unpackthepantry.groceries;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ergerodr on 3/6/16.
 */
public class HouseMate {
    private String mName;
    private List<HouseMate> mHouseMates;
    private int mItems; //Number of items this user has.

    public HouseMate(){
        mName = null;
        mHouseMates = new ArrayList<>();
        mItems = 0;
    }

    public List<HouseMate> getHouseMates() {
        return mHouseMates;
    }

    public void setHouseMates(List<HouseMate> houseMates) {
        mHouseMates = houseMates;
    }

    public int getItems() {
        return mItems;
    }

    public void setItems(int items) {
        mItems = items;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

}
