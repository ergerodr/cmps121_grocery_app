package com.unpackthepantry.groceries;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * Created by ergerodr on 3/5/16.
 */
public class GroceryPagerAdapter extends FragmentStatePagerAdapter{

    private List<GroceryItem> mGroceries;
    public GroceryPagerAdapter(FragmentManager fm){
        super(fm);
    }

    @Override
    public Fragment getItem(int position){
        GroceryItem item = mGroceries.get(position);
        return GroceryFragment.newInstance(item.getId());
    }

    @Override
    public int getCount(){
        return mGroceries.size();
    }
}
