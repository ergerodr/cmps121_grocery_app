package com.unpackthepantry.groceries;

import android.support.v4.app.Fragment;

/**
 * Created by ergerodr on 3/2/16.
 */
public class GroceryListActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() { return new GroceryListFragment(); }

}
