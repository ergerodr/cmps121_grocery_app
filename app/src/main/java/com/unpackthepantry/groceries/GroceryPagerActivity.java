package com.unpackthepantry.groceries;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import java.util.List;
import java.util.UUID;

public class GroceryPagerActivity extends AppCompatActivity {
    private static final String EXTRA_GROCERY_ID = "grocery_id ";
    public final String APP_TAG = "MyCameraApp";
    public final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1034;
    public String photoFileName = "photo.jpg";

    private ViewPager mViewPager;
    private List<GroceryItem> mGroceries;
    private ImageView itemImageView;

    public static Intent newIntent(Context packageContext, UUID groceryId){
        Intent intent = new Intent(packageContext, GroceryPagerActivity.class);
        intent.putExtra(EXTRA_GROCERY_ID, groceryId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grocery_pager);

        //get the saved groceryId from the intent
        UUID groceryId = (UUID) getIntent().getSerializableExtra(EXTRA_GROCERY_ID);

        mViewPager = (ViewPager) findViewById(R.id.grocery_view_pager);

        mGroceries = GroceryManager.get(this).getGroceryItems();
        FragmentManager fragmentManager = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                GroceryItem item = mGroceries.get(position);
                return GroceryFragment.newInstance(item.getId());
            }

            @Override
            public int getCount() {
                return mGroceries.size();
            }
        });

        for(int i = 0; i < mGroceries.size(); i++){
            if(mGroceries.get(i).getId().equals(groceryId)){
                mViewPager.setCurrentItem(i);
                mGroceries.get(i).setBitmap(mGroceries.get(i).getBitmap());
                break;
            }
        }
    }

}
