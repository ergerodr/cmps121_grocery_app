package com.unpackthepantry.groceries;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.unpackthepantry.groceries.response.BasicResponse;
import com.unpackthepantry.groceries.response.ResultList;
import com.unpackthepantry.groceries.response.ServerResponse;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;

public class userSetup extends AppCompatActivity implements FoundDialog.AddDialogListener{
    // Use URL below for production
    //private final String baseUrl = "http://960.aaramirez.tk/api/groceriesapi/";
    // Use URL below for testing purposes
    private final String baseUrl = "http://192.168.50.100:8000/groceriesapi/groceriesapi/";

    private final String LOG_TAG = "userSetup";
    private String userToken;
    private NavigationView nvDrawer;
    private DrawerLayout mDrawer;
    private Toolbar mToolbar;
    private ActionBarDrawerToggle mDrawerToggle;

    SharedPreferences sharedPref;

    private OkHttpClient httpClient;
    private HttpLoggingInterceptor logging;
    Retrofit retrofit;
    groceryService service;

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        //The action bar home/up action should open or close the drawer.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState){
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState(); //--------------HAMBURGER MENU!
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig){
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    private void setupDrawerContent(NavigationView navView){
        View headerLayout = navView.inflateHeaderView(R.layout.nav_header);
        if (headerLayout != null) {
            if(headerLayout.getParent() != null){
                ((ViewGroup)headerLayout.getParent()).removeView(headerLayout);
            }
        }
        navView.addHeaderView(headerLayout);
        navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                selectDrawerItem(menuItem);
                return true;
            }
        });
    }

    public void selectDrawerItem(MenuItem menuItem){
        //Setup what NavBar Items do here!
        switch (menuItem.getItemId()){
            case R.id.nav_item_1:
                break;
            case R.id.nav_item_2:
                break;
            case R.id.nav_item_3:
                break;
            default:
                break;
        }
        // Highlight the selected item, update the title, and close the drawer
        setTitle(menuItem.getTitle());
        mDrawer.closeDrawers();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_setup);

        // Get App Preferences
        Context context = getApplicationContext();
        sharedPref = context.getSharedPreferences(getString(R.string.preferenceFile), Context.MODE_PRIVATE);

        userToken = sharedPref.getString("userToken", "");
        Log.i(LOG_TAG, "Retrieved User Token: " + userToken);

        // Create HTTP Interface
        logging = new HttpLoggingInterceptor();
        // Set the desired logging level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();
        retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();
        service = retrofit.create(groceryService.class);

        /////////TOOLBAR CODE
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        mDrawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        nvDrawer = (NavigationView) findViewById(R.id.nav_view);

        setupDrawerContent(nvDrawer);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.drawer_open, R.string.drawer_close);
        mDrawer.setDrawerListener(mDrawerToggle);

    }

    public void createHouse(final View v) {
        // Generate a unique id, get user provided name, send to server
        SecureRandomString houseId = new SecureRandomString();
        EditText usrInput = (EditText) findViewById(R.id.usrText);
        String nickname = usrInput.getText().toString();
        Call<BasicResponse> queryResponseCall = service.addHouse(userToken, houseId.nextString(), nickname);
        usrInput.setText("");
        queryResponseCall.enqueue(new Callback<BasicResponse>() {
            @Override
            public void onResponse(Response<BasicResponse> response) {
                //Log.i(LOG_TAG, "Response: " + response.body().result);
                if (response.body().result.equals("ok")) {
                    Log.i(LOG_TAG, "House added successfuly");
                    Intent intent = new Intent(userSetup.this, GroceryListActivity.class);
                    startActivity(intent);
                } else {
                    int toastResId = R.string.toastResId;
                    Toast.makeText(userSetup.this, toastResId, Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Throwable t) {
                Log.i(LOG_TAG, "REQUEST FAILED");
            }
        });
    }

    public void findHouse(View v) {
        // Search for a pre-existing house by nickname
        // Display a dialog box depending on result
        final FoundDialog result = new FoundDialog();
        // Args used to select between success and failure messages
        final Bundle args = new Bundle();
        EditText usrInput = (EditText) findViewById(R.id.usrText);
        String nickname = usrInput.getText().toString();
        Call<ServerResponse> queryResponseCall = service.findHouse(userToken,nickname);
        usrInput.setText("");
        queryResponseCall.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Response<ServerResponse> response) {
                Log.i(LOG_TAG, response.body().result);
                // If house was found then set the appropriate arg
                if (response.body().result.equals("ok")) {
                    ResultList result = response.body().resultList.get(0);
                    Log.i(LOG_TAG,"id: " + result.houseId);
                    args.putInt("success", 1);
                    args.putString("houseId", result.houseId);
                } else {
                    args.putInt("success", 0);
                }
                result.setArguments(args);
                // Display dialog box
                result.show(getFragmentManager(), "FoundDialog");
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        Log.i(LOG_TAG, "Adding user to house!");
        String houseId = dialog.getArguments().getString("houseId", "");
        Log.i(LOG_TAG, "Test: " + houseId);

        Call<ServerResponse> queryResponseCall = service.addUser(userToken, houseId);
        queryResponseCall.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Response<ServerResponse> response) {
                if(response.body().result.equals("500")) { //If nothing was recieved from the server.
                    Toast.makeText(getApplicationContext(), R.string.server_error, Toast.LENGTH_LONG).show();
                }
                if (response.body().result.equals("ok")) {
                    Log.i(LOG_TAG, "User added to house!");
                    Intent intent = new Intent(userSetup.this, GroceryListActivity.class);
                    startActivity(intent);
                }

                //Log.i(LOG_TAG, response.body().resultList.toString());
                //Log.i(LOG_TAG, response.body().resultList.get(0).houseId);
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });

    }


}
