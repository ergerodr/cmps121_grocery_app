package com.unpackthepantry.groceries;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;


/**
 * Created by Aaron Ramirez on 0011 Mar 11 2016.
 */
public class FoundDialog extends DialogFragment {
    // Creating a listener for the ok button
    public interface AddDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog);
    }
    AddDialogListener mListener;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final int success = getArguments().getInt("success", 0);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Choose either success or failure message
        if (success == 1) {
            builder.setTitle(getString(R.string.houseFoundTitle));
            builder.setMessage(getString(R.string.houseFoundMsg));
        } else {
            builder.setTitle(getString(R.string.houseNotFoundTitle));
            builder.setMessage(getString(R.string.houseNotFoundMsg));
        }
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (success == 1) {
                    mListener.onDialogPositiveClick(FoundDialog.this);
                } else {
                    dismiss();
                }
            }
        });

        if (success == 1) {
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dismiss();
                }
            });
        }
        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (AddDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "must implement addDialogListener");
        }
    }
}
