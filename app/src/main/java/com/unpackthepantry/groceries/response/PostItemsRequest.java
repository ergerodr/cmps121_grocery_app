package com.unpackthepantry.groceries.response;

/**
 * Created by ergerodr on 27/10/16.
 */

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("org.jsonschema2pojo")
public class PostItemsRequest {

    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("resultList")
    @Expose
    private List<ItemList> resultList = new ArrayList<ItemList>();

    /**
     *
     * @return
     *     The result
     */
    public String getResult() {
        return result;
    }

    /**
     *
     * @param result
     *     The result
     */
    public void setResult(String result) {
        this.result = result;
    }

    /**
     *
     * @return
     *     The resultList
     */
    public List<ItemList> getResultList() {
        return resultList;
    }

    /**
     *
     * @param resultList
     *     The resultList
     */
    public void setResultList(List<ItemList> resultList) {
        this.resultList = resultList;
    }

}
