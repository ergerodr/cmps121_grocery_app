package com.unpackthepantry.groceries.response;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by ergerodr on 3/13/16.
 */
public class GetItemsResponse {

    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("resultList")
    @Expose
    private List<ItemList> ItemList = new ArrayList<ItemList>();

    /**
     *
     * @return
     *     The result
     */
    public String getResult() {
        return result;
    }

    /**
     *
     * @param result
     *     The result
     */
    public void setResult(String result) {
        this.result = result;
    }

    /**
     *
     * @return
     *     The itemList
     */
    public List<ItemList> getItemList() {
        return ItemList;
    }

    /**
     *
     * @param ItemList
     *     The itemList
     */
    public void setItemList(List<ItemList> ItemList) {
        this.ItemList = ItemList;
    }
}
