package com.unpackthepantry.groceries.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("org.jsonschema2pojo")
public class HouseInfoResponse {

    @SerializedName("houseId")
    @Expose
    private String houseId;
    @SerializedName("result")
    @Expose
    private String result;


    public String getHouseId() {
        return houseId;
    }

    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
