package com.unpackthepantry.groceries.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("org.jsonschema2pojo")
public class ResultList {

    @SerializedName("houseId")
    @Expose
    public String houseId;

}