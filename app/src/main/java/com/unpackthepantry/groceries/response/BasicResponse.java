package com.unpackthepantry.groceries.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("org.jsonschema2pojo")
public class BasicResponse {
    @SerializedName("result")
    @Expose
    public String result;

}
