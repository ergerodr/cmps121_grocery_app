package com.unpackthepantry.groceries.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Created by ergerodr on 3/13/16.
 */
public class ItemList {
    @SerializedName("groceryId")
    @Expose
    private String groceryId;
    @SerializedName("communal")
    @Expose
    private Boolean communal;
    @SerializedName("houseId")
    @Expose
    private String houseId;
    @SerializedName("itemOwner")
    @Expose
    private String itemOwner;
    @SerializedName("numRemaining")
    @Expose
    private String numRemaining;
    @SerializedName("dateCreated")
    @Expose
    private String dateCreated;
    @SerializedName("ownerId")
    @Expose
    private String ownerId;
    @SerializedName("itemName")
    @Expose
    private String itemName;

    /**
     *
     * @return
     *     The communal
     */
    public Boolean getCommunal() {
        return communal;
    }

    /**
     *
     * @param communal
     *     The communal
     */
    public void setCommunal(Boolean communal) {
        this.communal = communal;
    }

    /**
     *
     * @return
     *     The houseId
     */
    public String getHouseId() {
        return houseId;
    }

    /**
     *
     * @param houseId
     *     The houseId
     */
    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }

    /**
     *
     * @return
     *     The itemOwner
     */
    public String getItemOwner() {
        return itemOwner;
    }

    /**
     *
     * @param itemOwner
     *     The itemOwner
     */
    public void setItemOwner(String itemOwner) {
        this.itemOwner = itemOwner;
    }

    /**
     *
     * @return
     *     The numRemaining
     */
    public String getNumRemaining() {
        return numRemaining;
    }

    /**
     *
     * @param numRemaining
     *     The numRemaining
     */
    public void setNumRemaining(String numRemaining) {
        this.numRemaining = numRemaining;
    }

    /**
     *
     * @return
     *     The dateCreated
     */
    public String getDateCreated() {
        return dateCreated;
    }

    /**
     *
     * @param dateCreated
     *     The dateCreated
     */
    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     *
     * @return
     *     The ownerId
     */
    public String getOwnerId() {
        return ownerId;
    }

    /**
     *
     * @param ownerId
     *     The ownerId
     */
    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    /**
     *
     * @return
     *     The groceryId
     */
    public String getGroceryId() {
        return groceryId;
    }

    /**
     *
     * @param groceryId
     *     The id
     */
    public void setGroceryId(String groceryId) {
        this.groceryId = groceryId;
    }

    /**
     *
     * @return
     *     The itemName
     */
    public String getItemName() {
        return itemName;
    }

    /**
     *
     * @param itemName
     *     The itemName
     */
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

}
