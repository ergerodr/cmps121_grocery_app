package com.unpackthepantry.groceries.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class ServerResponse {
    @SerializedName("resultList")
    @Expose
    public List<ResultList> resultList = new ArrayList<ResultList>();

    @SerializedName("result")
    @Expose
    public String result;

}
