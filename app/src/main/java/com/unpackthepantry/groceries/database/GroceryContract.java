package com.unpackthepantry.groceries.database;

import android.content.ContentUris;
import android.content.UriMatcher;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by ergerodr on 17/02/17.
 *
 * Defines all Tables and URIs needed for the ContentProvider
 */

public class GroceryContract {
    //The name for our ContentProvider (I.E., GroceryProvider)
    public static final String CONTENT_AUTHORITY =
            "com.unpackthepantry.groceries.database.provider";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    //Possible paths to be appended to the base URI for each of the different tables.
    public static final String PATH_TABLE_USERS = "/users";
    public static final String PATH_TABLE_ITEMS = "/items";
    public static final String PATH_TABLE_HOUSES = "/houses";

    /**
     * Create one class for each table that handles all information regarding the table schema and
     * the URIs related to it.
     *
     * The BaseColumns interface adds the two fields _ID and _COUNT. This will allow easier
     * mapping with the ContentProvider.
     */

    public static final class UserEntry implements BaseColumns{
        //Uri location for the table
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_TABLE_USERS).build();
        //CONTENT_URI--------->content://com.unpackthepantry.groceries.database/users

        //Special prefixes that specify if a URI returns a list or a specific item.
        public static final String CONTENT_TYPE =      //list
                "vnd.android.cursor.dir/" + CONTENT_URI + "/" + PATH_TABLE_USERS;
        public static final String CONTENT_ITEM_TYPE = //item
                "vnd.android.cursor.item/" + CONTENT_URI + "/" + PATH_TABLE_USERS;


        //Define the table schema
        public static final String TABLE_NAME = "userTable";
        public static final String COLUMN_HOUSE_ID = "houseId";
        public static final String COLUMN_FIRST_NAME = "firstName";
        public static final String COLUMN_LAST_NAME = "lastName";
        public static final String COLUMN_ACCEPTED = "accepted";//I.E., user associated with house
        public static final String COLUMN_PROF_PIC = "profile_pic";

        // function to build a URI to find a specific user by his/her identifier
        //
        // returns "content://AUTH/user/#"
        public static Uri buildUserUri(long id){
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static final class GroceryItemEntry implements BaseColumns{
            /*
             *  returns "content://AUTH/user/#/item"
             */
            public static Uri getContentUri(long userId){
                return Uri.withAppendedPath(UserEntry.buildUserUri(userId), "items");
            }
        }
    }

    public static final class HouseEntry implements BaseColumns{
        //Uri location for the table
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_TABLE_HOUSES).build();

        //Special prefixes that specify if a URI returns a list or a specific item.
        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_URI + "/" + PATH_TABLE_HOUSES;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_URI + "/" + PATH_TABLE_HOUSES;

        //Define the table schema
        public static final String TABLE_NAME = "houseTable";
        public static final String COLUMN_NICKNAME = "nickName";
        public static final String COLUMN_MANAGER = "manager";

        //function to build a URI to find a specific user by his/her identifier
        public static Uri buildHousesUri(long id){
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static final class GroceryItemEntry implements BaseColumns{
            /*
             *  returns "content://AUTH/houses/#/items"
             */
            public static Uri getContentUri(long houseId){
                return Uri.withAppendedPath(HouseEntry.buildHousesUri(houseId), "items");
            }
        }

        public static final class UserEntry implements BaseColumns {
            /*
             *  returns "content://AUTH/houses/#/users"
             */
            public static Uri getContentUri(long houseId){
                return Uri.withAppendedPath(HouseEntry.buildHousesUri(houseId), "users");
            }
        }
    }

    public static final class GroceryItemEntry implements BaseColumns{
        //Uri location for the table
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_TABLE_ITEMS).build();

        //Special prefixes that specify if a URI returns a list or a specific item.
        public static final String CONTENT_TYPE =      //list
                "vnd.android.cursor.dir/" + CONTENT_URI + "/" + PATH_TABLE_ITEMS;
        public static final String CONTENT_ITEM_TYPE = //item
                "vnd.android.cursor.item/" + CONTENT_URI + "/" + PATH_TABLE_ITEMS;


        //Define the table schema
        public static final String TABLE_NAME = "itemTable";
        public static final String COLUMN_ITEM_OWNER = "itemOwner";
        public static final String COLUMN_HOUSE_ID = "houseId";
        public static final String COLUMN_ITEM_NAME = "itemName";
        public static final String COLUMN_COMMUNAL = "communal";
        public static final String COLUMN_REMAINING = "numRemaining";
        public static final String COLUMN_DATE_CREATED = "dateCreated";//I.E., user associated with house
        public static final String COLUMN_ITEM_PIC = "itemPicture";

        //function to build a URI to find a specific user by his/her identifier
        public static Uri buildItemsUri(long id){
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

}
