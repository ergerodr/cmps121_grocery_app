package com.unpackthepantry.groceries.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by ergerodr on 17/02/17.
 */

public class GroceryDBHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "householdInventory.db";
    public static final int DATABASE_VERSION = 1;

    public GroceryDBHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        addHouseTable(db);
        addUserTable(db);
        addGroceryItemTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /* COMPLETE USER TABLE SCHEMA

        " (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
        " houseId, " +
        " firstName TEXT NOT NULL DEFAULT '0', " +
        " lastName TEXT NOT NULL DEFAULT '0', " +
        " accepted BOOLEAN DEFAULT '0', " +
        " profile_pic TEXT, " +
        " FOREIGN KEY(houseId) references houseTable(id) ON DELETE CASCADE); " ;
     */
    private void addUserTable(SQLiteDatabase db){
        db.execSQL(
                "CREATE TABLE " + GroceryContract.UserEntry.TABLE_NAME + " (" +
                        GroceryContract.UserEntry._ID + " INTEGER PRIMARY KEY, " +
                        GroceryContract.UserEntry.COLUMN_HOUSE_ID + " , " +
                        GroceryContract.UserEntry.COLUMN_FIRST_NAME + " TEXT NOT NULL DEFAULT '0', " +
                        GroceryContract.UserEntry.COLUMN_LAST_NAME + " TEXT NOT NULL DEFAULT '0', " +
                        GroceryContract.UserEntry.COLUMN_ACCEPTED + " BOOLEAN DEFAULT '0', " +
                        GroceryContract.UserEntry.COLUMN_PROF_PIC + " BLOB, " +
                        "FOREIGN KEY (" + GroceryContract.UserEntry.COLUMN_HOUSE_ID + ") " +
                            "REFERENCES " + GroceryContract.HouseEntry.TABLE_NAME +
                            " (" + GroceryContract.HouseEntry._ID + ") ON DELETE CASCADE);"
        );
    }


    /* COMPLETE GROCERYITEM TABLE SCHEMA

        " (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
        " itemOwner " +
        " house_id INTEGER " +
        " itemName TEXT NOT NULL DEFAULT '0'," +
        " communal BOOLEAN DEFAULT '0', " +
        " numRemaining INTEGER DEFAULT '0'," +
        " dateCreated DATE NOT NULL DEFAULT (datetime('now','localtime')), " +
        " itemPicture TEXT, " +
        " FOREIGN KEY(itemOwner) reference userTable(id) ON DELETE CASCADE); " +
    */
    private void addGroceryItemTable(SQLiteDatabase db){
        db.execSQL(
                "CREATE TABLE " + GroceryContract.GroceryItemEntry.TABLE_NAME + " (" +
                        GroceryContract.GroceryItemEntry._ID + " INTEGER PRIMARY KEY, " +
                        GroceryContract.GroceryItemEntry.COLUMN_ITEM_OWNER + ", " +
                        GroceryContract.GroceryItemEntry.COLUMN_HOUSE_ID + " INTEGER, " +
                        GroceryContract.GroceryItemEntry.COLUMN_ITEM_NAME + " TEXT NOT NULL DEFAULT '0', " +
                        GroceryContract.GroceryItemEntry.COLUMN_COMMUNAL + " BOOLEAN DEFAULT '0', " +
                        GroceryContract.GroceryItemEntry.COLUMN_REMAINING + " INTEGER DEFAULT '0', " +
                        GroceryContract.GroceryItemEntry.COLUMN_DATE_CREATED + " DATE NOT NULL DEFAULT (datetime('now','localtime')), " +
                        GroceryContract.GroceryItemEntry.COLUMN_ITEM_PIC + " TEXT, " +
                        "FOREIGN KEY (itemOwner) " +
                        "REFERENCES " + GroceryContract.UserEntry.TABLE_NAME + " (" + GroceryContract.UserEntry._ID + ") ON DELETE CASCADE);"
        );
    }

    /*  COMPLETE HOUSE TABLE SCHEMA

        " (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
        " nickName TEXT NOT NULL DEFAULT '0', " +   //This will be the name of the House
        " manager TEXT NOT NULL DEFAULT '0');";

     */

    private void addHouseTable(SQLiteDatabase db){
        db.execSQL(
                "CREATE TABLE " + GroceryContract.HouseEntry.TABLE_NAME + " (" +
                        GroceryContract.HouseEntry._ID + " INTEGER PRIMARY KEY, " +
                        GroceryContract.HouseEntry.COLUMN_NICKNAME + " TEXT NOT NULL DEFAULT '0', " +
                        GroceryContract.HouseEntry.COLUMN_MANAGER + " TEXT NOT NULL DEFAULT '0');"
        );
    }
}
