package com.unpackthepantry.groceries.database;

import android.annotation.TargetApi;
import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import com.unpackthepantry.groceries.BuildConfig;
import com.unpackthepantry.groceries.database.GroceryContract.HouseEntry;
import com.unpackthepantry.groceries.database.GroceryContract.GroceryItemEntry;
import com.unpackthepantry.groceries.database.GroceryContract.UserEntry;


/**
 * Created by ergerodr on 12/24/16.
 */

public class GroceryProvider extends ContentProvider {
    public static final int HOUSE = 100;
    public static final int HOUSE_ID = 101;
    public static final int HOUSE_ID_ITEMS = 102;
    public static final int HOUSE_ID_USERS = 103;

    public static final int ITEM = 200;
    public static final int ITEM_ID = 201;

    public static final int USER = 300;
    public static final int USER_ID = 301;
    public static final int USER_ID_ITEMS = 302;

    private static final UriMatcher mUriMatcher = buildUriMatcher();
    private GroceryDBHelper groceryDBHelper = null;

    /**
     * Builds a UriMatcher that is used to determine witch database request is being made.
     */
    public static UriMatcher buildUriMatcher(){
        String content = GroceryContract.CONTENT_AUTHORITY;

        // All paths to the UriMatcher have a corresponding code to return
        // when a match is found (the ints above).
        UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

        uriMatcher.addURI(content, GroceryContract.PATH_TABLE_HOUSES, HOUSE);
        uriMatcher.addURI(content, GroceryContract.PATH_TABLE_HOUSES + "/#", HOUSE_ID);
        uriMatcher.addURI(content, GroceryContract.PATH_TABLE_HOUSES + "/#/items", HOUSE_ID_ITEMS);
        uriMatcher.addURI(content, GroceryContract.PATH_TABLE_HOUSES + "/#/users", HOUSE_ID_USERS);

        uriMatcher.addURI(content, GroceryContract.PATH_TABLE_ITEMS, ITEM);
        uriMatcher.addURI(content, GroceryContract.PATH_TABLE_ITEMS + "/#", ITEM_ID);

        uriMatcher.addURI(content, GroceryContract.PATH_TABLE_USERS, USER);
        uriMatcher.addURI(content, GroceryContract.PATH_TABLE_USERS + "/#", USER_ID);
        uriMatcher.addURI(content, GroceryContract.PATH_TABLE_USERS + "/#/items", USER_ID_ITEMS);

        return uriMatcher;
    }

    @Override
    public boolean onCreate() {
        groceryDBHelper = new GroceryDBHelper(getContext());
        return true;
    }

    @Override
    public String getType(Uri uri) {
        switch(mUriMatcher.match(uri)){
            case HOUSE:    //returns list of houses
                return HouseEntry.CONTENT_TYPE;
            case HOUSE_ID: //returns a specific house, with more details
                return HouseEntry.CONTENT_ITEM_TYPE;
            case HOUSE_ID_ITEMS:
                return GroceryItemEntry.CONTENT_TYPE;
            case HOUSE_ID_USERS:
                return UserEntry.CONTENT_TYPE;
            case USER:     //returns a list of users
                return UserEntry.CONTENT_TYPE;
            case USER_ID:  //returns a specific user
                return UserEntry.CONTENT_ITEM_TYPE;
            case USER_ID_ITEMS:
                return GroceryItemEntry.CONTENT_TYPE;
            case ITEM:     //returns a list of items
                return GroceryItemEntry.CONTENT_TYPE;
            case ITEM_ID:  //returns a specific item.
                return GroceryItemEntry.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Cursor query(Uri uri, //-----------------FROM table_name
                        String[] projection, //-----Column(s) to select
                        String selection, //--------WHERE col = value
                        String[] selectionArgs,//---replaces ? placeholders in the selection clause
                        String sortOrder) { //------ORDER BY col,col,...
        Cursor retCourser;
        SQLiteDatabase db = groceryDBHelper.getReadableDatabase();
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        switch(mUriMatcher.match(uri)) {
            case HOUSE:
                queryBuilder.setTables(GroceryContract.HouseEntry.TABLE_NAME);
                break;
            case HOUSE_ID:
                queryBuilder.setTables(GroceryContract.HouseEntry.TABLE_NAME);
                queryBuilder.appendWhere(GroceryContract.HouseEntry._ID + " = "
                        + uri.getLastPathSegment());
                break;
            case HOUSE_ID_ITEMS:
                queryBuilder.setTables(GroceryContract.GroceryItemEntry.TABLE_NAME);
                queryBuilder.appendWhere(GroceryItemEntry.COLUMN_HOUSE_ID + " = "
                        + uri.getLastPathSegment());
                break;
            case HOUSE_ID_USERS:
                queryBuilder.setTables(GroceryContract.UserEntry.TABLE_NAME);
                queryBuilder.appendWhere(UserEntry.COLUMN_HOUSE_ID + " = "
                        + uri.getLastPathSegment());
                break;
            case USER:
                queryBuilder.setTables(GroceryContract.UserEntry.TABLE_NAME);
                break;
            case USER_ID:
                queryBuilder.setTables(GroceryContract.UserEntry.TABLE_NAME);
                queryBuilder.appendWhere(UserEntry._ID + " = "
                        + uri.getLastPathSegment());
                break;
            case USER_ID_ITEMS:
                queryBuilder.setTables(GroceryContract.GroceryItemEntry.TABLE_NAME);
                queryBuilder.appendWhere(GroceryItemEntry.COLUMN_ITEM_OWNER + " = "
                        + uri.getLastPathSegment());
                break;
            case ITEM:
                queryBuilder.setTables(GroceryContract.GroceryItemEntry.TABLE_NAME);
                break;
            case ITEM_ID:
                queryBuilder.setTables(GroceryContract.GroceryItemEntry.TABLE_NAME);
                queryBuilder.appendWhere(GroceryItemEntry._ID + " = "
                        + uri.getLastPathSegment() );
                break;
            default:
                throw new UnsupportedOperationException("Unsupported uri: " + uri);
        }

        logQuery(queryBuilder,  projection, selection, sortOrder);

        logQueryDeprecated(queryBuilder, projection, selection, sortOrder);

        Cursor cursor = queryBuilder.query(db, projection, selection, selectionArgs,
                null, null, sortOrder);
        // if we want to be notified of any changes:
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    private void logQuery(SQLiteQueryBuilder builder, String[] projection, String selection, String sortOrder) {
        if (BuildConfig.DEBUG) {
            Log.v("cpsample", "query: " + builder.buildQuery(projection, selection, null, null, sortOrder, null));
        }
    }

    @SuppressWarnings("deprecation")
    private void logQueryDeprecated(SQLiteQueryBuilder builder, String[] projection, String selection, String sortOrder) {
        if (BuildConfig.DEBUG) {
            Log.v("cpsample", "query: " + builder.buildQuery(projection, selection, null, null, null, sortOrder, null));
        }
    }
    /*If a column name is not in the ContentValues argument, you may want to provide a default value
     *for it either in your provider code or in your database schema.
     *
     *Method returns URI for the new row inserted.
     */
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = groceryDBHelper.getWritableDatabase();
        long id;
        switch (mUriMatcher.match(uri)){
            case USER:
                id = db.insert(UserEntry.TABLE_NAME, null, values);
                return getUriforId(id, uri);
            case ITEM:
                id = db.insert(GroceryItemEntry.TABLE_NAME, null, values);
                return getUriforId(id, uri);
            case HOUSE:
                id = db.insert(HouseEntry.TABLE_NAME, null, values);
                return getUriforId(id, uri);
            default:
                throw new UnsupportedOperationException("Unsupported URI: " + uri);
        }
    }

    private Uri getUriforId(long id, Uri uri){
        if (id > 0){
            Uri itemUri = ContentUris.withAppendedId(uri, id);
            getContext().getContentResolver().notifyChange(itemUri, null);
            return uri;
        }
        throw new SQLiteException("Problem while inserting into URI: " + uri);
    }

    @Override
    public int delete(Uri uri,
                      String selection,        //Column to select on
                      String[] selectionArgs) {//Value to compare to
        SQLiteDatabase db = groceryDBHelper.getWritableDatabase();
        int deleteCount = 0;
        String id, where;
        switch(mUriMatcher.match(uri)){
            case USER_ID:
                id = uri.getLastPathSegment();
                where = UserEntry._ID + " = " + id;
                if(!selection.isEmpty()){
                    where += " AND " + selection;
                }
                deleteCount = db.delete(UserEntry.TABLE_NAME, where, selectionArgs);
                break;
            case ITEM_ID:
                id = uri.getLastPathSegment();
                where = GroceryItemEntry._ID + " = " + id;
                if(!selection.isEmpty()){
                    where += " AND " + selection;
                }
                deleteCount = db.delete(GroceryItemEntry.TABLE_NAME, where, selectionArgs);
                break;
            case HOUSE_ID: //Need to check if manager is issuing this call and there are no members.
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        if(deleteCount > 0){
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return deleteCount;
    }

    public int update(Uri uri,                 //Table to update
                      ContentValues values,    //Columns to update
                      String selection,        //Column to select on
                      String[] selectionArgs) {// Value to compare to
        SQLiteDatabase db = groceryDBHelper.getWritableDatabase();
        int updateCount = 0;
        String id, where;
        switch(mUriMatcher.match(uri)){
            case USER_ID:
                id = uri.getLastPathSegment();
                where = UserEntry._ID + " = " + id;
                if(!selection.isEmpty()){
                    where += " AND " + selection;
                }
                updateCount = db.update(UserEntry.TABLE_NAME, values, where, selectionArgs);
                break;
            case ITEM_ID:
                id = uri.getLastPathSegment();
                where = GroceryItemEntry._ID + " = " + id;
                if(!selection.isEmpty()){
                    where += " AND " + selection;
                }
                updateCount = db.update(GroceryItemEntry.TABLE_NAME, values, where, selectionArgs);
                break;
            case HOUSE_ID:
                id = uri.getLastPathSegment();
                where = HouseEntry._ID + " = " + id;
                if(!selection.isEmpty()){
                    where += " AND " + selection;
                }
                updateCount = db.update(HouseEntry.TABLE_NAME, values, where, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        if(updateCount > 0){
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return updateCount;
    }

}
