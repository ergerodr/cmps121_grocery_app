package com.unpackthepantry.groceries;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.ContentObserver;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.UUID;

import com.google.firebase.iid.FirebaseInstanceId;
import com.unpackthepantry.groceries.database.GroceryContract;
import com.unpackthepantry.groceries.response.GetItemsResponse;
import com.unpackthepantry.groceries.response.HouseInfoResponse;
import com.unpackthepantry.groceries.response.ItemList;
import com.unpackthepantry.groceries.response.PostItemsRequest;


import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by ergerodr on 3/2/16.
 */
public class GroceryListFragment extends Fragment {
    // Use URL below for production
    //private final String baseUrl = "http://960.aaramirez.tk/api/groceriesapi/";
    // Use URL below for testing purposes
    private final String baseUrl = "http://192.168.50.100:8000/groceriesapi/groceriesapi/";

    private final String LOG_TAG = "GroceryListFragment";
    private OkHttpClient httpClient;
    private HttpLoggingInterceptor logging;
    Retrofit retrofit;
    groceryService service;
    SharedPreferences sharedPref;
    private String userToken;
    private String houseId;
    private List<ItemList> houseItems;

    private RecyclerView mRecyclerView;
    private GroceryAdapter mAdapter;
    private NavigationView mNavView;
    private DrawerLayout mDrawerLayout;
    private Toolbar mToolbar;
    public ActionBarDrawerToggle mDrawerToggle;
    private ContentObserver mObserver;

    ////////////////////////ACTION/TOOL BAR CODE////////////////
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mObserver = new ContentObserver(new Handler(Looper.getMainLooper())) {
            @Override
            public void onChange(boolean selfChange) {
                super.onChange(selfChange);
                Log.d(LOG_TAG, "ContentObserver Updating UI");
                updateUI();
            }
        };
        getContext().getContentResolver().registerContentObserver(GroceryContract.BASE_CONTENT_URI, false, mObserver);
    }

    @Override
    public void onDestroy(){
        getContext().getContentResolver().unregisterContentObserver(mObserver);
        super.onDestroy();
    }

    @Override
    public void onResume(){
        super.onResume();
        updateUI(); //updates RecyclerView
    }

    private void setupDrawerContent(NavigationView navView){
        View headerLayout = navView.inflateHeaderView(R.layout.nav_header);
        if (headerLayout != null) {
            if(headerLayout.getParent() != null){
                ((ViewGroup)headerLayout.getParent()).removeView(headerLayout);
            }
        }
        navView.addHeaderView(headerLayout);
        navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                selectDrawerItem(menuItem);
                return true;
            }
        });
    }

    public void selectDrawerItem(MenuItem menuItem){
        //Setup what NavBar Items do here!
        switch (menuItem.getItemId()){
            case R.id.firebase_token:
                // Get token
                String token = FirebaseInstanceId.getInstance().getToken();
                // Log and toast
                String msg = getString(R.string.msg_token_fmt, token);
                Log.d(LOG_TAG, msg);
                Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_item_1:
                break;
            case R.id.nav_item_2:
                break;
            case R.id.nav_item_3:
                break;
            default:
                break;

        }
        // Highlight the selected item, update the title, and close the drawer
        getActivity().setTitle(menuItem.getTitle());
        mDrawerLayout.closeDrawers();
    }

    @Override//method called when user presses menu item
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.menu_new_item://toolbar.xml
                GroceryItem grocery = new GroceryItem();
                ContentValues values = new ContentValues();
                GroceryManager.get(getActivity()).addGrocery(grocery);//adds to ArrayList of groceries.
                //getActivity().getContentResolver().insert();
                //saves groceryId into intent
                Intent intent = GroceryPagerActivity.newIntent(getActivity(), grocery.getId());
                startActivity(intent);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        mDrawerToggle.syncState(); //--------------HAMBURGER MENU!
    }

    //////////////////////////////UI CODE///////////////////////////////
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_grocery_list, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.grocery_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        updateUI();
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);

        mDrawerLayout = (DrawerLayout) view.findViewById(R.id.drawerLayout);
        mNavView = (NavigationView) view.findViewById(R.id.nav_view);

        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);

        setupDrawerContent(mNavView);

        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), mDrawerLayout, mToolbar, R.string.drawer_open, R.string.drawer_close);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        getHouseId();
        return view;
    }

    @Override//Inflate "toolbar.xml" as menu
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.toolbar, menu);
    }

    private void updateUI(){
        GroceryManager groceryManager = GroceryManager.get(getActivity());
        List<GroceryItem> items = groceryManager.getGroceryItems();
        if(mAdapter == null){
            mAdapter = new GroceryAdapter(items);
            mRecyclerView.setAdapter(mAdapter);
        } else {
            Log.d(LOG_TAG, "In function");
            mAdapter.setGroceryItems(items);
            mAdapter.notifyDataSetChanged();
            if (mAdapter.getItemCount() == 0){
                FrameLayout layout = (FrameLayout) getActivity().findViewById(R.id.frame_layout);
                Drawable pic = getResources().getDrawable(R.drawable.ic_empty_fridge); ///REQUR
                layout.setBackgroundDrawable(pic);
            } else {
                FrameLayout layout = (FrameLayout) getActivity().findViewById(R.id.frame_layout);
                layout.setBackgroundResource(R.drawable.ic_menu_add);
            }
        }
    }
    //////////////////////////JSON PARSING AND HTTP CALL CODE////////////////////////
    private String retroFitSetup(){
        // Get App Preferences
        Context context = getActivity().getApplicationContext();
        sharedPref = context.getSharedPreferences(getString(R.string.preferenceFile), Context.MODE_PRIVATE);

        userToken = sharedPref.getString("userToken", "");
        houseId = sharedPref.getString("houseId", "");
        Log.i(LOG_TAG, "Retrieved User Token: " + userToken);

        // Create HTTP Interface
        logging = new HttpLoggingInterceptor();
        // Set the desired logging level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();
        retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();
        service = retrofit.create(groceryService.class);

        return userToken;
    }

    private void updateItemsOnServer(String itemName, int numRemaining, boolean communal){
        String userToken = retroFitSetup();
        Call<PostItemsRequest> addItemCall = service.addUserItem(userToken, itemName, numRemaining, communal);
        addItemCall.enqueue(new Callback<PostItemsRequest>() {
            @Override
            public void onResponse(Response<PostItemsRequest> response) {
                if (response.body().getResult().equals("ok")) {
                    Log.i(LOG_TAG, "Items forwarded to server");
                } else {
                    int toastResId = R.string.toastResId;
                    Toast.makeText(getActivity(), toastResId, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.i(LOG_TAG, "updateItemsOnServer(): FAILED");
            }
        });
    }

    private void getHouseId() {
        userToken = retroFitSetup();
        Call<HouseInfoResponse> getHouseInfoCall = service.getHouseInfo(userToken);

        getHouseInfoCall.enqueue(new Callback<HouseInfoResponse>() {
            @Override
            public void onResponse(Response<HouseInfoResponse> response) {
                if (response.body().getResult().equals("ok")) {
                    houseId = response.body().getHouseId();
                    Log.i(LOG_TAG, "House id from server: " + houseId);
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }


    ////////////////////////// RECYCLER VIEW CODE BELOW/////////////////////////
    private class GroceryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private GroceryItem mGroceryItem;
        private ImageView mGroceryItemView; //Need to add photos.
        private TextView mCaptionView;
        private TextView mDayCounter;
        private ImageButton mDeleteButton;

        //each view should have space for a picture, caption, and day counter.
        public GroceryHolder(final View itemView){
            super(itemView);

            itemView.setOnClickListener(this);
            mGroceryItemView = (ImageView) itemView.findViewById(R.id.groceryView);
            mCaptionView = (TextView) itemView.findViewById(R.id.captionView);
            mDayCounter = (TextView) itemView.findViewById(R.id.dayCounter);
            mDeleteButton = (ImageButton) itemView.findViewById(R.id.deleteButton);
        }

        //Set each element to a concrete value in each list item in RecyclerView.
        public void bindGrocery(final GroceryItem item){
            mGroceryItem = item;
            //Set mGroceryItem View with images here later
            mCaptionView.setText(mGroceryItem.getItemName());
            //TEMPORARILY SET TO "String.valueOf(0)" CHANGE TO UPDATE AFTER EXACTLY ONE DAY
            mDayCounter.setText(getString(R.string.daysOld, String.valueOf(0)));
            mDeleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GroceryManager.get(getActivity()).deleteGroceryItem(item.getId());
                    updateUI();

                }
            });
        }

        @Override
        public void onClick(View v){
            Intent intent = GroceryPagerActivity.newIntent(getActivity(), mGroceryItem.getId());
            startActivity(intent);
        }
    }

    private class GroceryAdapter extends RecyclerView.Adapter<GroceryHolder> {
        private List<GroceryItem> mGroceryItems;

        public GroceryAdapter (List<GroceryItem> items){ mGroceryItems = items; }

        @Override
        public GroceryHolder onCreateViewHolder(ViewGroup parent, int viewType){
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_grocery, parent, false);
            return new GroceryHolder(view);
        }

        @Override//update "View" data with "GroceryItem.class" data
        public void onBindViewHolder(GroceryHolder holder, int position){
            GroceryItem item = mGroceryItems.get(position);
            holder.bindGrocery(item);
            //updateItemsOnServer(item.getItemName(), item.getQuantity(), item.isCommunal());
        }

        @Override
        public int getItemCount(){ return mGroceryItems.size(); }

        public void setGroceryItems(List<GroceryItem> items) { mGroceryItems = items; }
    }
}
