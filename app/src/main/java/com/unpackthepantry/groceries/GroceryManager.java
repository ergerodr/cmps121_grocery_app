package com.unpackthepantry.groceries;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by ergerodr on 3/2/16.
 */
public class GroceryManager {
    private static GroceryManager sGroceryManager;
    private List<GroceryItem> mGroceries;
    private Context mContext;

    public static GroceryManager get (Context context){
        if(sGroceryManager == null){
            sGroceryManager = new GroceryManager(context);
        }
        return sGroceryManager;
    }

    private GroceryManager(Context context){
        mContext = context.getApplicationContext();
        mGroceries = new ArrayList<>();

    }

    public void addGrocery(GroceryItem item){
        mGroceries.add(item);
    }

    public List<GroceryItem> getGroceryItems(){
        return mGroceries;
    }

    public GroceryItem getGroceryItem(UUID id){
        for(GroceryItem item : mGroceries){
            if(item.getId().equals(id)){
                return item;
            }
        }
        return null;
    }

    public void deleteGroceryItem(UUID id){
        for(int i = 0; i < mGroceries.size(); i++){
            if(mGroceries.get(i).getId().equals(id)){
                mGroceries.remove(i);
                break;
            }
        }
    }

    public File getPhotoFile(GroceryItem item){
        File externalFilesDir = mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if(externalFilesDir == null){
            return null;
        }
        return new File(externalFilesDir, item.getPhotoFilename());
    }
}
